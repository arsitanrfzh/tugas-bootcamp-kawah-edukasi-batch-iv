package org.acme;

import javax.inject.Inject;
import javax.persistance.EntityManager;
import javax.validation.Valid;
import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskResource {
	connection = DriverManager.getConnection(url: "jdbc:postgresql://localhost:5432/kawahedukasi", user: "postgres", password: "postgres");
	} catch (SQLException e) {
		throw new RuntimeException(e);
}
	
	@Inject
	EntityManager em;
	
	public Connection connection = null;
	
	List<Task> tasks = new ArrayList<>();
	
	public Long id = 1L; //Mengenerated id yg menempel di Object secara otomatis
	
	//Menampilkan keseluruhan daftar object yang dimiliki
	@GET
	@Path("tasks")
	public List<Task> getTasks() {
		return tasks;
	}
	
	@GET
	@Path("{idTask}") //Menampilkan object dengan id tertentu
	public Object getTaskById(@PathParam("idTask") Long id) { 
		for(Task task : tasks) { //menampung tasks dari kumpulan task yg kita miliki
			if(task.getId() == id) {
				return task;
			}
		}
		
		HashMap<String, Object> result = new HashMap<>();
		result.put("message", "Task dengan id tersebut tidak ditemukan");
		
		return result;
	}
	
	//Menambahkan data object baru (melakukan validasi input)
	@POST
	public List<Task> addTask(@Valid Task task) { //Jika menggunakan @valid : Mengembalikan structure data balikannya
		task.setId(id++);
		tasks.add(task);
		return tasks;
	}
	
	//Mengupdate data object dengan id tertentu (melakukan validasi input)
	@PUT
	@Path("id")
	public HashMap<String, Object> editTask(Long id, @Valid Task taskBaru) {
		Boolean idAda = false;
		for (Task task : tasks) {
			if (task.getId() == id) {
				idAda = true;
				task.title = taskBaru.title;
				task.description = taskBaru.description;
				task.subject = taskBaru.subject;
				task.score = taskBaru.score;
			}
		}
		
		HashMap<String, Object> result = new HashMap<>(); //Penulisannya mirip MediaType, Application, Json yang bentuknya Key dan Value
		
		if (idAda == true) {
			result.put("message", "Data task berhasil diubah");
		} else {
			result.put("message", "Task dengan id tersebut tidak ditemukan");
		}
		return result;
	}
	
	
	//Menghapus data object dengan id tertentu
	@DELETE
	@Path("{id}")
	public HashMap<String, Object> deleteTaskById(Long id) {
		Boolean isAda = false;
		for (Task task : tasks) {
			if (task.getId() == id) {
				tasks.remove(task);
				isAda = true;
			}
		}
		
		HashMap<String, Object> result = new HashMap<>(); //Penulisannya mirip MediaType, Application, Json yang bentuknya Key dan Value
		if (isAda == true) {
			result.put("message", "Data task berhasil dihapus");
		} else {
			result.put("message", "Task dengan id tersebut tidak ditemukan");
		}
		return result;
	}
	
	//Menghapus keseluruhan data object yang dimiliki dan mengembalikan Id menjadi 1
	@DELETE
	@Path("{tasks}")
	public HashMap<String, Object> deleteTasks() {
		tasks.clear();
		id = 1L;
		HashMap<String, Object> result = new HashMap<>(); //Penulisannya mirip MediaType, Application, Json yang bentuknya Key dan Value
		em.createQuery("SELECT FROM Task t WHERE t.id = :angka").setParameter("angka", id).executeUpdate();
		tasks = em.createQuery("SELECT t FROM Task t", Task.class).getResultList();
		result.put("message", "Data keseluruhan task berhasil dihapus dan Id kembali menjadi 1");
		return result;
	}
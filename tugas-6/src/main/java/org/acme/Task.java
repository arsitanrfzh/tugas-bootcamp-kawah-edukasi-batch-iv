package org.acme;

package javax.validation.constraints;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonGetter;


public class Task {
	
	//Membuat minimal 4 atribut yang terdapat paling sedikit 1 tipe data String dan 1 tipe data yang termasuk Number
	private Long id;
	
	@NotBlank(message = "Title tidak boleh kosong") //Membuat validasi dan custom message ke minimal 4 atribut yang terdapat pada class yg dibuat
	public String title;
	@NotNull(message = "Description tidak boleh null")
	public String description;
	@NotBlank(message = "message tidak boleh kosong")
	public String subject;
	@Min(value = 0, message = "Score minimal adalah 0")
	@Max(value = 100, message = "Score maksimal adalah 100")
	public Integer score;
	
	@JsonGetter
	public Long getId() {
		return id;
	}
	
	@JsonIgnore
	public void setId(Long id) {
		this.id = id;
	}
}
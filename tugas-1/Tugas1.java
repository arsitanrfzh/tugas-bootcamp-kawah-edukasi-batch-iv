import java.util.Scanner; // import the Scanner class

public class Tugas1 {
  
  public static void main(String[] args) {
    
	Scanner input = new Scanner(System.in);
        int menu;
		System.out.println("Tugas 1 Bootcamp Backend Kawah Edukasi Batch iv");
		System.out.println("================================================");
		System.out.println("Menu");
		System.out.println("[1] Identitas");
		System.out.println("[2] Kalkulator");
		System.out.println("[3] Perbandingan");
		System.out.println("[4] Keluar");
		System.out.println("================================================");
		
		// Enter Choice and press Enter
		System.out.print("Masukan Nomor Menu yang Ingin Anda Pilih (Ketik 1/2/3/4) : ");
        int pilihMenu = input.nextInt();
		
		if(pilihMenu == 1) {
			// If User Choose 1, the result is identity program
			System.out.println("----------Identitas---------");
			System.out.println("Nama : Arsita Nurfauziah");
			System.out.println("Alasan : Kebawa arus. Setelah iseng daftar kawah edukasi, baru liat bootcampnya hanya buat BE, seru sih belajarnya jadi ngerasa tertantang");
			System.out.println("Ekspektasi : Bisa mendalami BE, dan bisa bekerja sebagai BE/Software Developer");
		} else if (pilihMenu == 2) {
			// If User Choose 2, the result is calculator program
			System.out.println("-------------Kalkulator------------");
			System.out.println("1. Penjumlahan");
			System.out.println("2. Pengurangan");
			System.out.println("3. Pembagian");
			System.out.println("4. Perkalian");
			System.out.println("5. Sisa Bagi");
			
			// Operation Calculator Choice
				System.out.print("Masukan Pilihan Operas (Ketik 1/2/3/4/5): ");
				int pilihKalkulator = input.nextInt();
				
			// Enter Choice and press Enter
				System.out.print("Masukan Angka Pertama: ");
				float numbers1 = input.nextFloat();
				System.out.print("Masukan Angka Kedua: ");
				float numbers2 = input.nextFloat();

			String calculator = "";
			float result = 0;
			switch (pilihKalkulator)
        {
            case 1 : result = numbers1 + numbers2;
					calculator = "Penjumlahan";
					break;
            case 2 : result = numbers1 - numbers2; 
					calculator = "Pengurangan";
					break;
            case 3 : result = numbers1 / numbers2; 
					calculator = "Pembagian";
					break;
            case 4 : result = numbers1 * numbers2; 
					calculator = "Perkalian";
					break;
            case 5 : result = numbers1 % numbers2;
					calculator = "Sisa Bagi";
					break;
            default : System.out.println("Anda Salah Memasukan Pilihan, Harap Baca Pilihan Pada Menu");
        }
			System.out.println("Hasil dari operasi "+calculator+" adalah "+result);
		} else if (pilihMenu==3) {
			// If User Choose 3, the result is comparison program
            System.out.println("--------------Perbandingan--------------");
            System.out.println("Masukkan angka pertama: ");
            float comparison1 = input.nextFloat();
            System.out.println("Masukkan angka kedua: ");
            float comparison2 = input.nextFloat();
			
			if(comparison1 > comparison2) {
				System.out.println(comparison1 +" lebih besar dari : " +comparison2);
			} else if (comparison1 < comparison2) {
				System.out.println(comparison1 +" lebih kecil dari : " +comparison2);
			} else {
				System.out.println("Kedua bilangan sama");
            }

        } else if (pilihMenu==4) {
			// If User Choose 4, the result is Exit program
			System.out.println("Sampai Jumpa Lagi!");
        } else {
			System.out.println("Input yang Anda Masukan Salah");
		}
	}
}
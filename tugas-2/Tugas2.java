import java.util.Scanner; // import the Scanner class

public class Tugas2 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //membuat input Scanner
		
		int menu = 0;
		System.out.println("Tugas 2 Bootcamp Backend Kawah Edukasi Batch iv");
		System.out.println("================================================");
		System.out.println("---- MENU ----");
		System.out.println("[1] Nilai Mutlak");
		System.out.println("[2] Nilai Terbesar");
		System.out.println("[3] Perpangkatan");
		System.out.println("[99] Keluar");
		System.out.println("================================================");
		System.out.println("");
		
		// Enter Choice and Press Enter
		System.out.print("Masukan Nomor Menu yang Ingin Anda Pilih (Ketik 1/2/3/99) : ");
		
        int menuChoice = input.nextInt();
        //Input Pilihan Menu yang Diinginkan Oleh User
		
		if (menuChoice == 1){
            System.out.println("---------Nilai Mutlak----------");
            // Enter Number and Press Enter
            System.out.println("Silakan Masukkan Angka yang Anda Inginkan : ");
            System.out.println("");

            int valueA = input.nextInt();

            System.out.println("Nilai mutlak = "+ absoluteValue(valueA));  //Menginput Angka yang Diinginkan Oleh User
		} else if (menuChoice == 2) {  //Jika User Memilih Menu 2 Maka Akan Mengeluarkan Hasil Nilai Terbesar
            System.out.println("----------Menu Nilai Terbesar---------");
            // Enter Banyaknya Bilangan and Press Enter
            System.out.println("Silakan Masukkan Banyaknya Bilangan yang Anda Inginkan : ");
            System.out.println("");
			
            int lengthArray = input.nextInt();
            if (lengthArray == 0) { //Program Akan Dihentikan Jika User Memilih Angka 0
                return;
		} 
            int myArray[]= new int [lengthArray]; //Menginput Banyaknya Angka Yang Diinginkan Oleh User

            for (int i = 0; i < lengthArray; i++){
            //Menginput Angka Pilihan User yang Akan Dicari Nilai Terbesarnya
                System.out.println("Nilai ke-"+(i+1)+" = ");
                int valueA = input.nextInt();
                myArray[i]= valueA;
            }
            System.out.println("Jadi, Angka Terbesarnya adalah "+biggestNumber(myArray));
            
        } else if (menuChoice == 3){
            //If User Choose Menu 3, Maka Akan Menghasilkan Menu Perpangkatan
            System.out.println("");
            System.out.println("----------Menu Perpangkatan---------");
			// Enter Basis Angka and Press Enter
            System.out.println("Silakan Masukan Basis Angka yang Anda Inginkan : ");
            System.out.println("");

            int numberBase = input.nextInt(); // Menginput Basis Angka Untuk Menu Perpangkatan

            System.out.println("Silakan Masukan Nilai Pangkat yang Anda Inginkan : ");
            System.out.println("");

            int exponent = input.nextInt();  //Menginput Bilangan Eksponen Untuk Menu Perpangkatan

            System.out.println("Hasil perpangkatan = "+ rank(numberBase,exponent)); //Mencetak Hasil Perpangkatan
        } else if (menuChoice == 99) { 
			// If User Choose 99, the result is Exit program
			System.out.println("");
			System.out.println("Sampai Jumpa Lagi!");
        } else {
			System.out.println("Input yang Anda Masukan Salah");
	}
}

    public static int absoluteValue ( int valueNilaiMutlak) {
		//Method Untuk Menentukan Nilai Mutlak
        if (valueNilaiMutlak < 0){
            return -valueNilaiMutlak;
        } else {
            return valueNilaiMutlak;
        }
	}
    
    public static int biggestNumber (int arrayValue[]) {
		//Method Untuk Mencari Angka Terbesar
        int temp = arrayValue[0];
        //Value temp Berguna Sebagai Temporary Slot Untuk Bilangan yang Dibandingkan
		for (int j = 1; j < arrayValue.length; j++) {
			if(temp < arrayValue[j]){
                temp = arrayValue[j];
                //Jika Angka Pembanding(temp) Lebih Kecil Dari Angka Pada Komponen Array Ke-j, Maka Angka Pembanding(temp) Akan Digantikan Oleh Array Ke-j
            }
		}
        return temp;
	}

    public static float rank (float numberBase, int exponent) {
		//Method Untuk Perpangkatan, Tidak Berlaku Untuk Pangkat Pecahan(Bentuk Akar)
        float rankResult;
        if (exponent < 0){ // Untuk Pangkat Negatif

            rankResult = 1 / numberBase;
            //rankResult Mendefinisikan Variabel Untuk Menyimpan Hasil Iterasi Perkalian
            //Pangkat Negatif (a^-n) Artinya 1/a^n 
            float newNumberBase = 1/numberBase;
            int j = -1 * exponent;
            for (int i = 1; i < j; i++){
                rankResult = rankResult*newNumberBase;
            }
            
        } else if(exponent > 0) { //Untuk Pangkat Positif
            rankResult = numberBase;
            for (int i = 1; i < exponent; i++){
                rankResult = rankResult*numberBase;
            }
        } else {
            rankResult = 1; //Jika Pangkat 0, Maka Hasilnya Adalah 1
        }
        return rankResult;
	}
} 
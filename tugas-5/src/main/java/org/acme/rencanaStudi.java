package org.acme;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;

public class rencanaStudi {
	
    @NotBlank(message="Nama wajib diisi")
    @Length(min=2, max=33, message="Panjang nama 2-30 karakter")
    public String namaMahasiswa;

    @Min(value=3, message="Peminatan baru bisa diambil pada semester 3")
    @Max(value=14, message="Apakah Anda tidak ingin menikah saja?")
    public Integer semesterYangDiambil;

    @NotBlank(message="Pilihan peminatan pertama wajib diisi")
    public String pilihanPeminatanPertama;

    @NotBlank(message="Pilihan peminatan kedua wajib diisi")
    public String pilihanPeminatanKedua;

    public rencanaStudi(){

    }

    public rencanaStudi(String nama, Integer semester, String pilihanSatu, String pilihanDua){
        this.namaMahasiswa = nama;
        this.semesterYangDiambil = semester;
        this.pilihanPeminatanPertama = pilihanSatu;
        this.pilihanPeminatanKedua = pilihanDua;
    }
}
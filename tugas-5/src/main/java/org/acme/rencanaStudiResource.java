package org.acme;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@Path("/krs")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class rencanaStudiResource {

    public Map<Integer, rencanaStudi> kartuRencanaStudi = new HashMap<>();
    public Integer nim = 1;

    @Inject
    Validator validator;
	
    public rencanaStudiResource(){
        rencanaStudi rencana1 = new rencanaStudi("Mahasiswa 1",3,"Basis Data","Proses Bisnis");
        rencanaStudi rencana2 = new rencanaStudi("Mahasiswa 2",5,"Sistem Informasi Manajemen","Algoritma Pemrograman");
        kartuRencanaStudi.put(nim++, rencana1);
        kartuRencanaStudi.put(nim++, rencana2);
    }

    public static class Result {
		
        Result(String message) {
            this.success = true;
            this.message = message;
        }

        Result(Set<? extends ConstraintViolation<?>> violations) {
            this.success = false;
            this.message = violations.stream()
                    .map(cv -> cv.getMessage())
                    .collect(Collectors.joining(", "));
        }

        private String message;
        private boolean success;

        public String getMessage() {
            return message;
        }

        public boolean isSuccess() {
            return success;
        }
    }
	
    @GET
    public Map<Integer, rencanaStudi> getKartuRencanaStudi(){
        return kartuRencanaStudi;
    }

//Get by id
    @GET
    @Path("{nim}")
    public rencanaStudi getRencanaStudiByNIM( @PathParam("nim") Integer nim){
        return kartuRencanaStudi.get(nim);
    }

    @POST
    public Result addRencanaStudi(rencanaStudi rencanaStudiBaru){
        Set<ConstraintViolation<rencanaStudi>> violations = validator.validate(rencanaStudiBaru);
        if (violations.isEmpty()){
            kartuRencanaStudi.put(nim++,rencanaStudiBaru);
            return new Result("Rencana Studi baru telah ditambahkan");
        } else {
            return new Result(violations);
        }
    }
	
    @PUT
    @Path("{nim}")
    public Object updateRencanaStudi(@PathParam("nim") Integer nim,@Valid rencanaStudi newRencanaStudi){
        Map<String, rencanaStudi> pembaharuan = new HashMap<>();
        pembaharuan.put("KRS Lama", kartuRencanaStudi.get(nim));
        kartuRencanaStudi.replace(nim, newRencanaStudi);
        pembaharuan.put("pembaharuanRencanaStudi", kartuRencanaStudi.get(nim));
        return pembaharuan;
    }
	
    @DELETE
    @Path("{nim}")
    //Menghapus data object dengan id(nim) tertentu
    public rencanaStudi deleteRencanaStudi(@PathParam("nim") Integer nim){
        //Menyimpan data yang akan dihapus pada variabel sementara (temp)
        rencanaStudi temp = kartuRencanaStudi.get(nim);

        //Menghapus data dengan id(nim) tertentu
        kartuRencanaStudi.remove(nim);

        //Mengembalikan data yang telah dihapus
        return temp;
    }

    @DELETE
    @Path("emptyData")
    public String emptyRencanaStudi(){
        kartuRencanaStudi.clear();
        nim=1;
        return "Semua data telah terhapus";
    }
	
}